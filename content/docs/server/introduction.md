---
title: "Server"
description: "Reference for the Cactus Comments server."
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
draft: false
images: []
menu:
  docs:
    parent: "server"
weight: 31
toc: true
---

The server consists of two parts. The Matrix homeserver (typically `Synapse`), and the Cactus Comments application service (`cactus-appservice`). The appservice manages the comment section rooms on the homeserver, creating new ones on-demand when you need them. It also helps you manage moderation, by replicating bans and power levels across all comment section rooms. It provides a chatbot interface in the moderation room for you to interact with it.

You can use the public Cactus Comments server provided by us or host your own. Self-hosting allows you to be independent from any third party (in this case, us), at the cost of setting up and running your own server. See [Self-Hosting](../self-host) for more details.


## Architecture

The Cactus Comments application service implements the [Application Service API](https://matrix.org/docs/spec/application_service/r0.1.2). This means we have to expose an HTTP web server. We implemented our web server in the [flask microframework](https://flask.palletsprojects.com/) and the entire application in [Python](https://www.python.org/) 3.9. We interact with the Matrix homeserver with pure HTTP calls and not through an SDK using the [requests](https://requests.readthedocs.io/) HTTP library. The application is containerized with [docker](https://www.docker.com/). You can read and [contribute](../../community/contribute/) to the entire source code [here](https://gitlab.com/cactus-comments/cactus-appservice).
